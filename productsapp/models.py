from django.db import models


# Create your models here.
class Categories(models.Model):
    name = models.CharField(max_length=255)
    photo = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Products(models.Model):
    name = models.CharField(max_length=255)
    category = models.ForeignKey(Categories, on_delete=models.CASCADE)
    description = models.TextField()
    photo_url = models.CharField(max_length=255)

    def __str__(self):
        return self.name
