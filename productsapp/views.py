from django.contrib.auth import authenticate, logout, login
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib import messages
from .models import Categories, Products
from django.contrib.auth.decorators import login_required


# Create your views here.


def index(request):
    context = {
        'Categories': Categories.objects.all()
    }
    return render(request, 'Products/index.html', context)


def products_view(request, id):
    category = get_object_or_404(Categories, id=id)
    context = {
        'products': Products.objects.filter(category=category)
    }
    return render(request, 'Products/ViewProducts.html', context)


def login_view(request):
    if request.user.is_authenticated:
        return redirect(index)

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        next = request.POST.get('next')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if next:
                return redirect(next)
            return redirect(index)
        else:
            messages.error(request, 'Check Username And Password')
            return render(request, 'Products/login.html', {}, messages)
    return render(request, 'Products/login.html')


def signup_view(request):
    if request.user.is_authenticated:
        return redirect(index)

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        email_qs = User.objects.filter(email=email)
        username_qs = User.objects.filter(username=username)

        if (email_qs.exists()):
            messages.error(request, 'Email Already Exists!!!')
            return render(request, 'Products/signup.html', {}, messages)

        if (username_qs.exists()):
            messages.error(request, 'Username Already Exists!!!')
            return render(request, 'Products/signup.html', {}, messages)
        else:
            user_new = User.objects.create_user(username, email, password)
            user_new.save()
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect(index)
            else:
                return render(request, 'Products/signup.html')
    else:
        return render(request, 'Products/signup.html')



def logout_view(request):
    logout(request)
    return redirect(index)


@login_required(login_url='/login/')
def productsManagement(request):
    context = {
        'products': Products.objects.all()
    }
    return render(request, 'Products/AllProducts.html', context)


@login_required(login_url='/login/')
def addProduct(request):
    categories = Categories.objects.all()
    context = {
        'categories': categories
    }
    if request.method == 'POST':
        name = request.POST['name']
        category = request.POST['category']
        description = request.POST['description']
        product_qs = Products.objects.filter(name=name)

        if product_qs.exists():
            messages.error(request, 'Product Already Exists!!!')
            return render(request, 'Products/Add Products.html', context, messages)

        myfile = request.FILES['photo']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        photo_url = fs.url(filename)

        product_new = Products()
        product_new.name = name
        product_new.description = description
        product_new.category = Categories.objects.get(id=category)
        product_new.photo_url = photo_url
        product_new.save()

        return redirect(productsManagement)

    return render(request, 'Products/Add Products.html', context)


@login_required(login_url='/login/')
def categoryManagement(request):
    categories = Categories.objects.all()

    context = {
        'categories': categories
    }
    return render(request, 'Products/AllCategories.html', context)


@login_required(login_url='/login/')
def addCategory(request):
    if request.method == 'POST':
        name = request.POST['name']
        photo = request.FILES['photo']
        category_qs = Categories.objects.filter(name=name)

        if category_qs.exists():
            messages.error(request, 'Category Already Exists!!!')
            return render(request, 'Products/Add Category.html', {}, messages)

        fs = FileSystemStorage()
        filename = fs.save(photo.name, photo)
        uploaded_file_url = fs.url(filename)
        category_new = Categories()
        category_new.name = name
        category_new.photo = uploaded_file_url
        category_new.save()
        return redirect(categoryManagement)
    return render(request, 'Products/Add Category.html')
