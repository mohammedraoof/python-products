from django.urls import path
from productsapp import views

urlpatterns = [
   path('', views.index, name='index'),
   path('login/', views.login_view, name='login'),
   path('signup/', views.signup_view, name='signup'),
   path('logout/', views.logout_view, name='logout_view'),
   path('productmanagement/', views.productsManagement, name='productmanagement'),
   path('categorymanagement/', views.categoryManagement, name='categorymanagement'),
   path('addproduct/', views.addProduct, name='addproduct'),
   path('addcategory/', views.addCategory, name='addcategory'),
   path('viewproducts/<int:id>', views.products_view, name='viewproducts'),
]
